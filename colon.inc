%define prev 0

%macro colon 2
	%2: dq prev
	db %1, 0
	%define prev %2
%endmacro
