section .text
%include 'lib.inc'

global find_word
find_word:
.check_equals:
    add rsi, 8
    push rsi
    call string_equals
    pop rsi
    test rax, rax
    jnz .return_true

.next_word:
    sub rsi, 8
    mov rsi, [rsi]
    test rsi, rsi
    jnz .check_equals

.return_false:
    mov rax, 0
    ret

.return_true:
    sub rsi, 8
    mov rax, rsi
    ret
