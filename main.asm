%include 'colon.inc'
%include 'words.inc'
%include 'lib.inc'
extern find_word

section .data

size_err: db 'The length of the word is more than 255 characters', 0xA, 0
find_err: db 'There is no such word in the dictionary', 0xA, 0
buff: times 256 db 0

section .text
global _start
_start:
    mov rdi, buff
    mov rsi, 255
    call read_word
    test rax, rax
    jz .size_error

    mov rdi, rax
    mov rsi, prev

    call find_word
    test rax, rax
    jz .find_error

    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi    ; код возврата 0
    call exit

.size_error:
    mov rdi, size_err
    call print_stderr
    jmp .exit_program_with_err

.find_error:
    mov rdi, find_err
    call print_stderr
.exit_program_with_err:
    mov rdi, 1      ; пусть код возврата 1
    call exit

print_stderr:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax    ; string_length is stored in аккумулятор, rdx - length (bytes)
    mov rax, 1      ; "write" syscall
    mov rsi, rdi    ; data
    mov rdi, 2      ; stderr
    syscall
    ret