ASM = nasm
ASMFLAGS = -f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

start: main.o dict.o lib.o
	$(LD) -o program $^
