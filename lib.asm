global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy

section .text
 
; [DONE] Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60 ; "exit" syscall in linux
    syscall


; [DONE] Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    push rdi
.loop:
    cmp byte [rdi+rax], 0x0
    je .return_length
    inc rax
    jmp .loop

.return_length:
    pop rdi
    ret


; [DONE] Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax    ; string_length is stored in аккумулятор, rdx - length (bytes)
    mov rax, 1      ; "write" syscall
    mov rsi, rdi    ; data
    mov rdi, 1      ; stdout
    syscall         ; забыл тебя братишка
    pop rdi
    ret


; [DONE] Принимает код символа и выводит его в stdout
print_char:         ; rdi
    push rdi
    mov rsi, rsp    ; data; понапридумывали регистров
    mov rax, 1      ; "write" syscall
    mov rdi, 1      ; stdout
    mov rdx, 1      ; length (bytes)
    syscall
    pop rdi         ; https://youtu.be/UOxkGD8qRB4
    ret


; [DONE] Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret


; [DONE] Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rsp
    mov rax, rdi
    mov rcx, 10
    dec rsp
.loop:
    xor rdx, rdx
    div rcx
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    cmp rax, 0
    jnz .loop

.print:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsp
    ret


; [DONE] Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .print_positive
    neg rdi
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
.print_positive:
    call print_uint
    ret


; [DONE] Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rsi
    push rcx
    push rdi
    xor rax, rax
.compare:
    mov cl, byte[rdi]
    cmp cl, byte[rsi]
    je .next
    jmp .return

.next:
    cmp byte[rsi], 0x0
    je .return_true
    inc rdi
    inc rsi
    jmp .compare

.return_true:
    inc rax
.return:
    pop rdi
    pop rcx
    pop rsi
    ret

; [DONE] Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    push rsi
    push rcx

    mov rdi, 0 ; stdout
    mov rax, 0 ; "write" syscall
    push rax
    mov rdx, 1 ; length
    mov rsi, rsp ; указатель на выделенное место в стеке, чтобы записать туда символ и потом забрать
    syscall
    pop rax ; накостылили конечно

    pop rcx
    pop rsi
    pop rdi
    ret


; [DONE] Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
.filter_beginning:
    call read_char
    cmp rax, 0x9
    je .filter_beginning
    cmp rax, 0x20
    je .filter_beginning

.read:
    mov [rdi + rcx], rax
    cmp rax, 0x0
    je .end
    cmp rax, 0x10
    je .end
    cmp rax, 0xA
    je .end
    inc rcx
    cmp rcx, rsi
    je .return_zero
    call read_char
    jmp .read

.end:
    mov rax, 0
    mov rdx, rcx
    mov byte [rdi + rcx], 0
    mov rax, rdi
    ret

.return_zero:
    xor rax, rax
    ret


; [DONE] Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov rsi, 10
.loop:
    xor rcx, rcx
    mov cl, byte [rdi+rdx]
    sub cl, '0' ; сразу в числа
    cmp cl, 0
    jb .end
    cmp cl, 9
    ja .end
    inc rdx
    push rdx
    mul rsi
    add rax, rcx
    pop rdx
    jmp .loop

.end:
    ret


; [DONE] Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative_number
    call parse_uint ; в rax будет 0, если прочитать не удалось
    ret

.negative_number:
    inc rdi
    call parse_uint
    neg rax         ; 0 remains 0
    inc rdx
    ret


; [DONE] Принимает указатель на строку, указатель на буфер и длину буфера.  А КАК ПРИНИМАЕТ ТО?????
; А вот так
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - указатель на длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.loop:
    cmp rdx, rax
    je .return_zero      ; вернуть нуль
    mov cl, byte [rdi+rax]
    mov byte [rsi+rax], cl
    cmp cl, 0x0
    je .return_length           ; вернуть длину
    inc rax
    jmp .loop

.return_zero:
    xor rax, rax
.return_length:
    ret
